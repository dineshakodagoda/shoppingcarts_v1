﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingcartProj.BL.Exceptions
{
   public class OrderItemNotFoundException : Exception
    {
        public OrderItemNotFoundException() :base("order item is not found!")
        {

        }
    }
}
