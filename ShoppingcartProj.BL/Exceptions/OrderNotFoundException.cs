﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingcartProj.BL.Exceptions
{
   public class OrderNotFoundException : Exception
    {
        public OrderNotFoundException() :base ("Order cannot be found!")
        {

        }
    }
}
