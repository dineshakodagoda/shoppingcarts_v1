﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingcartProj.BL.Entity
{
   public class ProductBL
    {
        public int productid { get; set; }
        public string productName { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public decimal price { get; set; }
        public bool availability { get; set; }
    }

}
