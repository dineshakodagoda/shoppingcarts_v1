﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ShoppingcartProj.BL.Entity
{
   public class OrderBL
    {
        [Key]
        public int OrderId { get; set; }
        [Required]
        public DateTime OrderDate { get; set; }
        [Required]
        public List<OrderItemBL> orderItems { get; set; }

        [Required(ErrorMessage = "select a customer")]
        public int customerId { get; set; }
        [ForeignKey(nameof(customerId))]
        public virtual CustomerBL customer { get; set; }
    }
}
