﻿using ShoppingcartProj.BL.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingcartProj.BL.IServices
{
    public interface IOrderService
    {
        void AddOrder(OrderBL order);

        List<OrderBL> getAllOrder();
        //List<OrderBL> GetOrder(int id);

        OrderBL getOrderById(int id);
        void updateorder(OrderUBL orderbl);
        void Deleteorderline(int id);

        void deleteorder(int id);
    }
}
