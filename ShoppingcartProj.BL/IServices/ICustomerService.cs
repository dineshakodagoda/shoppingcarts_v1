﻿using ShoppingcartProj.BL.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingcartProj.BL.IServices
{
    public interface ICustomerService
    {
        List<CustomerBL> GetCustomers();

        CustomerBL GetCustomerById(int id);
        

    }
}
