﻿using ShoppingcartProj.BL.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingcartProj.BL.IServices
{
   public  interface IProductService
    {
        void addproduct(ProductBL porduct);
       List<ProductBL> GetAllProducts();
        ProductBL GetProductById(int id);
       
        void DeleteUpdateQty(OrderItemBL orderitem);
        void addqty(List<OrderItemBL> orderitem);


        void UpdateProductQuantity(List<OrderItemBL> orderitem);

    }
}
