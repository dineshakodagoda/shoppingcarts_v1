﻿using AutoMapper;
using ShoppingcartProj.BL.Entity;
using ShoppingcartProj.BL.IServices;
using ShoppingcartProj.DAL.Entity;
using ShoppingcartProj.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShoppingcartProj.BL.Services
{
    public class IProductServices : IProductService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public IProductServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }
        public void addproduct(ProductBL productbl)
        {
            unitOfWork.ProductRepository.Create(mapper.Map<Product>(productbl));
        }

      

        public List<ProductBL> GetAllProducts()
        {
            return mapper.Map<List<ProductBL>>(unitOfWork.ProductRepository.GetAll().ToList());
        }

        public ProductBL GetProductById(int id)
        {

            var product = unitOfWork.context.Products.FirstOrDefault(f => f.productid == id);
            return mapper.Map<ProductBL>(product);
        }

      
        public void DeleteUpdateQty(OrderItemBL orderitem)
        {
         
                var existingproduct = unitOfWork.context.Products.FirstOrDefault(i => i.productid == orderitem.productid);
                var existingqty = existingproduct.Quantity;

            var addingqty = orderitem.Quantity;

                var newqty = 0;
                newqty = existingqty + addingqty;
                existingproduct.Quantity = newqty;
                unitOfWork.ProductRepository.Update(existingproduct);
                unitOfWork.save();

           
        }

        public void addqty(List<OrderItemBL> orderitem)
        {
            foreach (var items in orderitem) {
                var existingproduct = unitOfWork.context.Products.FirstOrDefault(i => i.productid == items.productid);
                var existingqty = existingproduct.Quantity;

                var addingqty = items.Quantity;

                var newqty = 0;
                newqty = existingqty + addingqty;
                existingproduct.Quantity = newqty;
                unitOfWork.ProductRepository.Update(existingproduct);
                unitOfWork.save();

            }
        }

        public void UpdateProductQuantity(List<OrderItemBL> orderitem)
        {
            
            foreach (var items in orderitem)
            {
                var existingproduct = unitOfWork.context.Products.FirstOrDefault(i => i.productid == items.productid);
                var productqty = existingproduct.Quantity;
                var existingitem = unitOfWork.context.OrderItems.FirstOrDefault( o=> o.OrderItemId == items.OrderItemId);
                var existingqty = existingitem.Quantity;
                var addingqty = items.Quantity;
                var updateqty = existingqty - addingqty;
                var newqty = 0;
                newqty = productqty - (-updateqty);
                existingproduct.Quantity = newqty;
                unitOfWork.ProductRepository.Update(existingproduct);
                unitOfWork.save();

            }


        }




    }
}
