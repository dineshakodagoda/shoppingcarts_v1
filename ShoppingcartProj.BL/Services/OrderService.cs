﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ShoppingcartProj.BL.Entity;
using ShoppingcartProj.BL.Exceptions;
using ShoppingcartProj.BL.IServices;

using ShoppingcartProj.DAL.Entity;
using ShoppingcartProj.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShoppingcartProj.BL.Services
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        private readonly IProductService productService;

        public OrderService(IUnitOfWork unitOfWork, IMapper mapper, IProductService productService)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;

            this.productService = productService;
        }

        public void AddOrder(OrderBL order)
        {
            if (order == null)
            {
                throw new OrderNotFoundException();
            }

            using (var transaction = unitOfWork.context.Database.BeginTransaction())
            {
                 try
                {
                    foreach (var item in order.orderItems)
                    {
                     //assigning correct values for the added order
                        var id = item.productid;
                        var orderitem = productService.GetProductById(id);
                        var price = Convert.ToInt32(orderitem.price);
                        item.UnitPrice = price;
                    }
                    //Updating the product quantity
                    productService.UpdateProductQuantity(mapper.Map<List<OrderItemBL>>(order.orderItems));
                    var Order = mapper.Map<Order>(order);
                    unitOfWork.OrderRepository.Create(Order);
                    unitOfWork.save();

                    transaction.Commit();

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }//end of the add order method

        public void deleteorder(int id)
        {
            using (var transaction = unitOfWork.context.Database.BeginTransaction())
            {
                try
                {
                    List<OrderItemBL> tobedeleted = new List<OrderItemBL>();
                    var orderdel = unitOfWork.context.Orders.Include(i => i.orderItems).FirstOrDefault(o => o.OrderId == id);
                    productService.addqty(mapper.Map<List<OrderItemBL>>(orderdel));
                    unitOfWork.OrderRepository.DeleteById(id);
                    unitOfWork.save();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }//delete order

        public void Deleteorderline(int id)
        {
            var orderitem = unitOfWork.context.OrderItems.
                Include(i=>i.OrderId).FirstOrDefault(i => i.OrderItemId == id);
            if (orderitem == null)
            {
                throw new OrderItemNotFoundException();
            }
            using (var transaction = unitOfWork.context.Database.BeginTransaction())
            {
                try
                {
                    productService.DeleteUpdateQty(mapper.Map<OrderItemBL>(orderitem));
                    unitOfWork.OrderItemRepository.DeleteById(id);
                    unitOfWork.OrderRepository.Update(orderitem.Order);
                    unitOfWork.save();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        public List<OrderBL> getAllOrder()
        {
            return mapper.Map<List<OrderBL>>(unitOfWork.OrderRepository.GetAll().ToList());
        }

        public OrderBL getOrderById(int id)
        {
            var order = unitOfWork.context.Orders.Include(p => p.orderItems).FirstOrDefault(f => f.OrderId == id);
            return mapper.Map<OrderBL>(order);
        }

        public void updateorder(OrderUBL orderbl)
        {
            if (orderbl == null)
            {
                throw new OrderNotFoundException();
            }
            else
            {
                foreach (var items in orderbl.orderItems)
                {
                    if (items == null)
                    {
                        throw new OrderItemNotFoundException();
                    }
                }
            } // delete function 
            using (var transaction = unitOfWork.context.Database.BeginTransaction())
            {
                try
                {
                    List<OrderItemUBL> updateItemList = new List<OrderItemUBL>();
                    foreach (var items in orderbl.orderItems)
                    {
                         if (items.isDeleted == true)
                        {
                            var ToBeDeletedId = items.OrderItemId;
                            var item = unitOfWork.context.OrderItems
                                    .AsNoTracking().Include(o => o.Order).Include(p => p.Product)
                                    .FirstOrDefault(i => i.OrderItemId == ToBeDeletedId);
                            unitOfWork.OrderItemRepository.DeleteById(ToBeDeletedId);

                           // unitOfWork.OrderItemRepository.Update(mapper.Map<OrderItem>(item));
                        }
                        if (items.isDeleted == false)
                        {
                            var ToBeUpdatedId = items.productid;
                            var item = unitOfWork.context.Products
                              .AsNoTracking() .FirstOrDefault(i => i.productid == ToBeUpdatedId);
                            items.UnitPrice = Convert.ToInt32(item.price); 
                            updateItemList.Add(items);
                        }
                    }
                    orderbl.orderItems = updateItemList;
                    unitOfWork.OrderRepository.Update(mapper.Map<Order>(orderbl));
                    unitOfWork.save();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }

        }//end of the update function

    }
}
          

       

                


