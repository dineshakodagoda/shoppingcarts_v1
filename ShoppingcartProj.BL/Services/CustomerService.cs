﻿using AutoMapper;
using ShoppingcartProj.BL.Entity;
using ShoppingcartProj.BL.IServices;
using ShoppingcartProj.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingcartProj.BL.Services
{
    public class CustomerService : ICustomerService
    {
       
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;
       

        public CustomerService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;

            this.mapper = mapper;

        }
    
        public CustomerBL GetCustomerById(int id)
        {
            var customer = unitOfWork.context.Customers.FirstOrDefault(f => f.customerId == id);
            return mapper.Map<CustomerBL>(customer);
        }

     

        public List<CustomerBL> GetCustomers()
        {
            return mapper.Map<List<CustomerBL>>(unitOfWork.CustomerRepository.GetAll());
        }
    }
}

