﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ShoppingcartProj.DAL.Migrations
{
    public partial class product : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "availability",
                table: "Products");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "availability",
                table: "Products",
                nullable: false,
                defaultValue: false);
        }
    }
}
