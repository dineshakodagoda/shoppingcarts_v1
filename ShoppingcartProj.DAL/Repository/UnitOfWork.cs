﻿using ShoppingcartProj.DAL.DbContext;
using ShoppingcartProj.DAL.Entity;
using ShoppingcartProj.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingcartProj.DAL.Repository
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly AppDbContext db;
        private GenericRepository<Customer> customerRepository;
        private GenericRepository<Product> productRepository;
        private GenericRepository<Order> orderRepository;
        private GenericRepository<OrderItem> orderItemRepository;
        public AppDbContext Context => db as AppDbContext;

        public UnitOfWork(AppDbContext db)
        {
            this.db = db;
        }
        public GenericRepository<Customer> CustomerRepository
        {
            get
            {
                this.customerRepository = new GenericRepository<Customer>(db);
                return customerRepository;
            }

        }

        public GenericRepository<Order> OrderRepository
        {
            get
            {
                this.orderRepository = new GenericRepository<Order>(db);
                return orderRepository;
            }

        }
        public GenericRepository<Product> ProductRepository
        {
            get
            {
                this.productRepository = new GenericRepository<Product>(db);
                return productRepository;

            }

        }

        public GenericRepository<OrderItem> OrderItemRepository
        {

            get {

                this.orderItemRepository = new GenericRepository<OrderItem>(db);
                return orderItemRepository;

            }

        }
        public AppDbContext context => db as AppDbContext;

        public void save()
        {
            db.SaveChanges();
        }
        private bool disposed = false;


        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
