﻿using Microsoft.EntityFrameworkCore;
using ShoppingcartProj.DAL.DbContext;
using ShoppingcartProj.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingcartProj.DAL.Repository
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        internal AppDbContext Context;
        internal DbSet<TEntity> dbset;
        public GenericRepository(AppDbContext context)
        {
            Context = context;
            this.dbset = context.Set<TEntity>();
        }

      

        public virtual void Create(TEntity entity)
        {
            dbset.Add(entity);
        }

        public virtual void Delete(TEntity entity)
        {
            if (Context.Entry(entity).State == EntityState.Detached)
            {
                dbset.Attach(entity);
            }
            dbset.Remove(entity);
        }

        public virtual TEntity DeleteById(object id)
        {
            TEntity DeleteEntity = dbset.Find(id);
            dbset.Remove(DeleteEntity);
            return DeleteEntity;
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return dbset;
        }

        public virtual TEntity GetById(object id)
        {
            return dbset.Find(id);
        }

        public virtual void Update(TEntity entity)
        {
            dbset.Update(entity);
        }
    }
}
