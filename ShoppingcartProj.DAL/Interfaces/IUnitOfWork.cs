﻿using ShoppingcartProj.DAL.DbContext;
using ShoppingcartProj.DAL.Entity;
using ShoppingcartProj.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingcartProj.DAL.Interfaces
{
   public interface IUnitOfWork
    {
        GenericRepository<Customer> CustomerRepository { get; }
        GenericRepository<Order> OrderRepository { get; }
        GenericRepository<Product> ProductRepository { get; }
        GenericRepository<OrderItem> OrderItemRepository { get; }
        AppDbContext context { get; }

        void save();



    }
}
