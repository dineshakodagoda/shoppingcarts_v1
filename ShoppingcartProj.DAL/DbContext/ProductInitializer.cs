﻿using ShoppingcartProj.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShoppingcartProj.DAL.DbContext
{
   public static class ProductInitializer
    {
        public static void Procductinitialize(AppDbContext context)
        {

            if (!context.Products.Any())
            {
                context.AddRange
                    (
                        new Product { productName = "Sugar", Description = "this is sugar", Quantity = 100, price = 120 },
                         new Product { productName = "Salt", Description = "this is salt", Quantity = 100, price = 100 },
                         new Product { productName = "Rice", Description = "this is rice", Quantity = 100, price = 60 },
                         new Product { productName = "Cookies", Description = "this is Cookie", Quantity = 100, price = 150 }
                      );
                context.SaveChanges();



            }

        }


    }
}
