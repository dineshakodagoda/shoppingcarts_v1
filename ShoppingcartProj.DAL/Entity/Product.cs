﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ShoppingcartProj.DAL.Entity
{
    public class Product
    {
        [Key]
        public int productid { get; set; }
        public string productName { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public decimal price { get; set; }
        public bool availability { get { return Quantity > 0; }  }
    }
}
