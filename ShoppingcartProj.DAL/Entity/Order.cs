﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using ShoppingcartProj.DAL.DbContext;

namespace ShoppingcartProj.DAL.Entity
{
   public class Order
    {
        [Key]
        public int OrderId { get; set; }
       
        [Required]
        public DateTime OrderDate { get; set; }
      
        [Required]
       public List<OrderItem> orderItems { get; set; }

        [Required(ErrorMessage = "select a customer")]
        public int customerId { get; set; }

        [ForeignKey(nameof(customerId))]
        public virtual Customer customer { get; set; }

        
    }
}
