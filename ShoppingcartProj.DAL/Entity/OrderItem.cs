﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ShoppingcartProj.DAL.Entity
{
    public class OrderItem
    {
        public int OrderItemId { get; set; }

       
        public int productid { get; set; }

        [ForeignKey(nameof(productid))]
        public virtual Product Product { get; set; }
        public int OrderId { get; set; }

        [ForeignKey(nameof(OrderId))]
        public virtual Order Order { get; set; }


        [Required]
        [Range(maximum: 10, minimum: 0, ErrorMessage = "Maximum amount is 10")]
        public int Quantity { get; set; }
        public int UnitPrice { get; set; }
        

    }
}
