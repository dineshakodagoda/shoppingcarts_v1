﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingcartProj.DAL.Entity
{
    public class Customer
    {
        public int customerId { get; set; }

        public string Name { get; set; }

        public string phonenumber { get; set; }

        public string Address { get; set; }
    }
}
