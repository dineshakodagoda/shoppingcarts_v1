﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ShoppingcartProj.DAL.DbContext;




namespace ShoppingcartProj
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();
            using (var scope =host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
            try
                {
                    var context = services.GetRequiredService<AppDbContext>();
                    DbInitializer.initialize(context);
                   
                }
                catch
                {

                }
            }
            
            var hostnew = CreateWebHostBuilder(args).Build();
            using (var scope = hostnew.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    var context = services.GetRequiredService<AppDbContext>();
                    ProductInitializer.Procductinitialize(context);

                }
                catch
                {

                }
            }
            hostnew.Run();
            host.Run();
        }

       

    public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
             WebHost.CreateDefaultBuilder(args)
                 .UseStartup<Startup>();
}
}
