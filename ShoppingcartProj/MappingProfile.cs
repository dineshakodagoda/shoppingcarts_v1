﻿using ShoppingcartProj.BL.Entity;
using ShoppingcartProj.DAL.Entity;
using ShoppingcartProj.Entity;
using ShoppingcartProj.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingcartProj
{
    public class MappingProfile :AutoMapper.Profile
    {
        public MappingProfile()
        {
            CreateMap<ProductPL,ProductBL>().ReverseMap();
            CreateMap<Product, ProductBL>().ReverseMap();
            CreateMap<ProductViewModel, ProductBL>().ReverseMap();

            CreateMap<OrderItemPL, OrderItemBL>().ReverseMap();
            CreateMap<OrderItem, OrderItemBL>().ReverseMap();
            CreateMap<orderlineViewmodel, OrderItemBL>().ReverseMap();
            CreateMap<UpdateOrderLine, OrderItemUBL>().ReverseMap();
            CreateMap<OrderItem, OrderItemUBL>().ReverseMap();



            CreateMap<OrderPL, OrderBL>().ReverseMap();
            CreateMap<Order, OrderBL>().ReverseMap();

            CreateMap<OrderViewModel, OrderBL>().ReverseMap();
            CreateMap<Order, OrderBL>().ReverseMap();

            CreateMap<orderdetailsviewmodel, OrderBL>().ReverseMap();
            CreateMap<updateorderviewmodel, OrderBL>().ReverseMap();

            CreateMap<OrderUpdateViewModel, OrderUBL>().ReverseMap();
            CreateMap<Order, OrderUBL>().ReverseMap();

            CreateMap<SaveOrderViewModel,OrderBL>().ReverseMap();
         CreateMap<SaveLineViewModel, OrderItemBL>().ReverseMap();

            CreateMap<CustomerPL, CustomerBL>().ReverseMap();
            CreateMap<Customer, CustomerBL>().ReverseMap();



        }

    }
}
