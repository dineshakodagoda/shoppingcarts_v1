﻿using ShoppingcartProj.Entity;
using System;
using System.Collections.Generic;

namespace ShoppingcartProj.ViewModel
{
    public class updateorderviewmodel
    {
        public int OrderId { get; set; }
        public DateTime OrderDate { get; set; }
        //public int TotalAmount { get; set; }
        public List<orderlineViewmodel> orderItems { get; set; }
        public int customerId { get; set; }

        public List<ProductPL> productlist{ get; set; }


    }
}
