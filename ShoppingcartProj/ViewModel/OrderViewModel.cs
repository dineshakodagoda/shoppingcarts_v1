﻿using ShoppingcartProj.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingcartProj.ViewModel
{
    public class OrderViewModel
    {
       // public int Id { get; set; }
       [Required]
        public int customerId { get; set; }

       // public int OrderId { get; set; }

       public DateTime OrderDate { get; set; }

       [Required]
      public List<OrderItemPL> orderItems { get; set; }

        //public int count { get; set; }
    }
}
