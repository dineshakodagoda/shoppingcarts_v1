﻿using ShoppingcartProj.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingcartProj.ViewModel
{
    public class AddingOrderViewModel
    {
        /*  public int ProductId { get; set; }
          public List<CustomerPL> CustomerNameSelect { get; set; }
          public List<ProductPL> SelectedProductName { get; set; }

          public string ProductDescription { get; set; }
          */
        public int ProductId { get; set; }
        public List<CustomerPL> CustomerList { get; set; }
        public List<ProductPL> productList { get; set; }
       // public List<OrderItemPL> orderitems { get; set; }

        [Required]
        public int Customerid { get; set; }



        [Required]
        public string ProductName { get; set; }

        public int UnitPrice { get; set; }

        [Required]
        public int Quantity { get; set; }

        //public int ProductPrice { get; set; }

        public string ProductDescription { get; set; }

        public string OrderDate { get; set; }

      
    }
}
