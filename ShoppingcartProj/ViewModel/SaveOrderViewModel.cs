﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingcartProj.ViewModel
{
    public class SaveOrderViewModel
    {
        public int CustomerId { get; set; }

        public DateTime OrderDate { get; set; }
        public List<SaveLineViewModel> orderItems { get; set; }

    }
}
