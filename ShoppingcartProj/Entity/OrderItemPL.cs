﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingcartProj.Entity
{
    public class OrderItemPL
    {

        public int productid { get; set; }

        [Required]
        [Range(maximum: 10, minimum: 0, ErrorMessage = "Maximum amount is 10")]
        public int Quantity { get; set; }
        public int UnitPrice { get; set; }

        



    }
}
