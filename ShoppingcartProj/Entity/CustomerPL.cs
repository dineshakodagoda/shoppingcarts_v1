﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingcartProj.Entity
{
    public class CustomerPL
    {
        public int customerId { get; set; }

        public string Name { get; set; }

        public string phonenumber { get; set; }

        public string Address { get; set; }
    }
}
