﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingcartProj.Entity
{
    public class OrderPL
    {
        public int OrderId { get; set; }

        [Required]
        public DateTime OrderDate { get; set; }
       
        [Required]
        public List<OrderItemPL> orderItems { get; set; }

        [Required(ErrorMessage = "select a customer")]
        public int customerId { get; set; }

      
    }
}
