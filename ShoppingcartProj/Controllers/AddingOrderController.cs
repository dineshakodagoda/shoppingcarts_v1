﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ShoppingcartProj.BL.Entity;
using ShoppingcartProj.BL.IServices;
using ShoppingcartProj.BL.Services;
using ShoppingcartProj.DAL.DbContext;
using ShoppingcartProj.Entity;
using ShoppingcartProj.ViewModel;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ShoppingcartProj.Controllers
{
    public class AddingOrderController : Controller
    {
        private readonly IMapper mapper;
        private readonly ICustomerService customerService;
        private readonly IProductService productService;
        private readonly IOrderService orderservice;

        public AddingOrderController(IMapper mapper, ICustomerService customerService,
         IProductService productService, IOrderService orderservice)
        {
            this.mapper = mapper;
            this.customerService = customerService;
            this.productService = productService;
            this.orderservice = orderservice;
        }

        // GET: /<controller>/
        [HttpGet]
        public IActionResult Index()
        {
            AddingOrderViewModel model = new AddingOrderViewModel();
            model.CustomerList = mapper.Map<List<CustomerPL>>(customerService.GetCustomers());
            model.productList = mapper.Map<List<ProductPL>>(productService.GetAllProducts());
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddCart([FromBody]OrderViewModel modelsave)
        {
            if (ModelState.IsValid)
            { 
                var order = mapper.Map<OrderBL>(modelsave);
                orderservice.AddOrder(order);
            }
            else {

                TempData["Success"] = false;
            }
            return RedirectToAction("Index", "AddingOrder");
        }

        [HttpGet]
        public IActionResult Orderlist()
        {
            OrderlistViewModel orderlist = new OrderlistViewModel();
            orderlist.orderlist = mapper.Map<List<OrderPL>>(orderservice.getAllOrder());
             return View(orderlist);
        }

        [HttpGet]
        public IActionResult OrderDetails(int id)
        {
          var model = mapper.Map<OrderPL>(orderservice.getOrderById(id));
          return View(model);
        }

        [HttpGet]
        public IActionResult updateOrder(int id)
        {
            var model = new updateorderviewmodel();
             model = mapper.Map<updateorderviewmodel>(orderservice.getOrderById(id));
            model.productlist = mapper.Map<List<ProductPL>>(productService.GetAllProducts());
            return View(model);
        }

        [HttpPost]
        public IActionResult saveorder([FromBody]OrderUpdateViewModel orderupdate)
        {
            var updatedorder = mapper.Map<OrderUBL>(orderupdate);
            orderservice.updateorder(updatedorder);
            return RedirectToAction("Orderlist", "AddingOrder");
        }

        [HttpPost]
        public IActionResult deleteorderline([FromBody]int id)
        {
            orderservice.Deleteorderline(id);
            return Ok();
        }

        public IActionResult deleteOrder(int id)
        {

            orderservice.deleteorder(id);
            return RedirectToAction("Orderlist", "AddingOrder");
        }

    }
}
