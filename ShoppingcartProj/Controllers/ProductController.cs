﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ShoppingcartProj.BL.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingcartProj.Controllers
{
    public class ProductController :Controller
    {
        private readonly IProductService productService;
        private readonly IMapper mapper;

        public ProductController(IProductService productService , IMapper mapper)
        {
            this.productService = productService;
            this.mapper = mapper;
        }

    }
}
