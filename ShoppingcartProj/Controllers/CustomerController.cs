﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ShoppingcartProj.BL.Services;
using ShoppingcartProj.ViewModel;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ShoppingcartProj.Controllers
{
    public class CustomerController : Controller
    {
        private readonly CustomerService service;
        private readonly IMapper mapper;

        public CustomerController(CustomerService service,IMapper mapper)
        {
            this.service = service;
            this.mapper = mapper;
        }
        // GET: /<controller>/
        [HttpGet]
        public IActionResult Index()
        {
            var model = new AddingOrderViewModel();
            return View(model);
        }
       
    }
}
